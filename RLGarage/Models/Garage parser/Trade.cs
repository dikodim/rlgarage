﻿using System.Collections.Generic;

namespace RL_Garage.Garage_parser
{
    public class Trade
    {
        public Links Link { get; set; }
        public string Platform { get; set; }
        public List<GarageItem> itemlist = new List<GarageItem>();
    }

    public class Want : Trade
    {
    }

    public class Have : Trade
    {
    }
}
