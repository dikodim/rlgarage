﻿using System.Collections.Generic;
using System;
using Leaf.xNet;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using RL_Garage.Insider_parser;
using System.Linq;
using Garage.Models.Garage_parser;
using Garage.Models;
using System.Threading.Tasks;

namespace RL_Garage.Garage_parser
{
    public class PageParser
    {
        const int TradesConstant = 21;

        private string UpdatePage = "https://rocket-league.com/trading";

        public List<string> ReturnList;

        public List<string> Parse()
        {

            ReturnList = new List<string>();

            ReturnList.Clear();

            using (HttpRequest request = new HttpRequest())
            {
                string page = request.Get(UpdatePage).ToString();

                HtmlDocument document = new HtmlDocument();
                document.LoadHtml(page);


                //Got 40 trade windows from UpdatePage (20 Have and 20 Want)
                for (int i = 1; i < TradesConstant; i++)
                {
                    Trade TradeHave = new Have();
                    Trade TradeWant = new Want();

                    //Need to parse each item here in foreach
                    HtmlNodeCollection collectionWant = document.DocumentNode.SelectNodes($"(//*[@id='rlg-theiritems'])[{i}]/a");
                    HtmlNodeCollection collectionHave = document.DocumentNode.SelectNodes($"(//*[@id='rlg-youritems'])[{i}]/a");
                    HtmlNodeCollection collectionLinks = document.DocumentNode.SelectNodes($"(//*[@class='rlg-trade-link-area-read rlg-trade-link-area-read-limit'])[{i}]");

                    TradeWant = ParseTrade(collectionWant);
                    TradeHave = ParseTrade(collectionHave);
                    TradeHave.Link = ParsePlayerData(collectionLinks);
                    TradeWant.Link = ParsePlayerData(collectionLinks);

                    int FullCostHave = 0;
                    int FullCostWant = 0;

                    foreach (var GarageItem in TradeWant.itemlist)
                    {
                        GarageItem.Itemname.Replace(" (Black Market)", "").Replace(" (Boost)", "");
                        int cost = GetPriceByItemName(GarageItem.Itemname, GarageItem.Color);
                        GarageItem.CreditValue = cost;
                        FullCostWant += GarageItem.CreditValue * GarageItem.Count;
                    }

                    foreach (var GI in TradeHave.itemlist)
                    {
                        GI.Itemname.Replace(" (Black Market)", "").Replace(" (Boost)", "");
                        int cost = GetPriceByItemName(GI.Itemname, GI.Color);
                        GI.CreditValue = cost;
                        FullCostHave += GI.CreditValue * GI.Count;
                    }

                    //Compare each item in trade
                    if (TradeHave.itemlist.Count == TradeWant.itemlist.Count)
                    {
                        for (int j = 0; j < TradeHave.itemlist.Count; j++)
                        {
                            var Hitem = TradeHave.itemlist[j];
                            var Witem = TradeWant.itemlist[j];

                            if (Witem.CreditValue < 0 || Hitem.CreditValue < 0)
                                continue;

                            if ((Hitem.CreditValue * Hitem.Count) > (Witem.CreditValue * Witem.Count))
                            {
                                //Got Profit Trade
                                using (EFDatabase blcontext = new EFDatabase())
                                {
                                    using (var context = new SaveTrades())
                                    {
                                        var ToSave = new Garage.Models.Garage_parser.Trade
                                        {
                                            Have = Hitem.Itemname,
                                            Want = Witem.Itemname,
                                            TradeLink = TradeHave.Link.TradeLink,
                                            Platform = TradeHave.Link.Steam,
                                            HavePrice = Hitem.Count * Hitem.CreditValue,
                                            WantPrice = Witem.Count * Witem.CreditValue
                                        };

                                        /*if (blcontext.Bots.Any(q => q.Blacklink == ToSave.Platform))
                                            continue;*/

                                        if (ToSave.Platform == null)
                                            continue;

                                        if (ToSave.Platform.Contains("playstation") || ToSave.Platform.Contains("xbox") || ToSave.Platform.Contains("switch"))
                                            continue;

                                        context.SavedTrades.Add(ToSave);
                                        context.SaveChanges();
                                    }
                                }

                                ReturnList.Add($"[H] {Hitem.Itemname} credit: {Hitem.CreditValue * Hitem.Count} - [W] {Witem.Itemname} credit: {Witem.CreditValue * Witem.Count}" +
                                    $" Tradelink: https://rocket-league.com{TradeHave.Link.TradeLink} Steamlink: {TradeHave.Link.Steam}");
                            }

                        }
                    }

                }

            }
            return ReturnList;

        }

        public static int GetPriceByItemName(string ItemName, string color)
        {
            using (EFDatabase context = new EFDatabase())
            {

                var entity = context.DatabaseItems.Where(q => q.Itemname != null && q.Itemname == ItemName && q.Color == color).FirstOrDefault();

                if (entity != null)
                {
                    return entity.MinPrice;
                }
                else
                {
                    return -1;
                }
            }
        }

        public Links ParsePlayerData(HtmlNodeCollection ItemNodes)
        {
            string htmldata = ItemNodes[0].InnerHtml;
            string[] d = htmldata.Substrings("href=\"", "\"");
            if (d.Length < 3)
            {
                return new Links { TradeLink = d[0], InventoryLink = d[1] };
            }
            return new Links { TradeLink = d[0], Steam = d[1], InventoryLink = d[2] };
        }

        public int ReturnItemCount(string expression)
        {
            if (expression == null || expression == "")
                return 1;

            var result = Regex.Match(expression, @"-?\d+(?:\.\d+)?").Value;
            if (result == "")
            {
                return 1;
            }

            return (int)Convert.ToDouble(result);
        }

        public Trade ParseTrade(HtmlNodeCollection ItemNodes)
        {
            Trade tempTrade = new Trade();

            foreach (var n in ItemNodes)
            {
                GarageItem tGarageItem = new GarageItem();

                string HtmlItem = n.InnerHtml;
                int count = 1;
                string color = "Default";
                string itemname = "";

                itemname = HtmlItem.Substring("<h2>", "</h2>");
                count = ReturnItemCount(HtmlItem.Substring("<div class=\"rlg-trade-display-item__amount", "</div>"));

                if (HtmlItem.Contains("rlg-trade-display-item-paint"))
                {
                    color = HtmlItem.Substring("data-name=\"", "\"></div>");
                }

                tempTrade.itemlist.Add(new GarageItem { Color = color, Count = count, Itemname = itemname });

            }
            return tempTrade;
        }
    }
}
