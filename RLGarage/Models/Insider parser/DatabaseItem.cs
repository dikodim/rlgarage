﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.Models.Insider_parser
{
    public class DatabaseItem
    {
        public string Color { get; set; }
        public int Id { get; set; }
        public string Itemname { get; set; }
        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }
    }
}