﻿using System;
using System.Collections.Generic;

namespace RL_Garage.Insider_parser
{
    public class Item
    {
        public List<Tuple<string, string>> Prices = new List<Tuple<string, string>>();
        public int Itemid { get; set; }
        public string Itemname { get; set; }
    }
}
